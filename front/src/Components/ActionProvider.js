import axios from "axios";
import React, { Component } from "react";

class ActionProvider {
  constructor(createChatBotMessage, setStateFunc) {
    this.createChatBotMessage = createChatBotMessage;
    this.setState = setStateFunc;
  }

  greet() {
    const greetingMessage = this.createChatBotMessage("Hi, friend.");
    this.updateChatbotState(greetingMessage);
  }

  updateChatbotState(message) {
    this.setState((prevState) => ({
      ...prevState,
      messages: [...prevState.messages, message],
    }));
  }

  handleLogin = () => {
    const message = this.createChatBotMessage("Logged in Successfully", {
      widget: "checkbalance",
    });
    this.updateChatbotState(message);
  };

  setUsername(message) {
    console.log(message);
    localStorage.setItem("username", message);
  }
  setPassword(message) {
    console.log(message);
    localStorage.setItem("password", message);
  }
  login = async () => {
    console.log("Hiii clicked login...");
    await axios
      .post("http://localhost:8181/signing/login", {
        username: localStorage.getItem("username"),
        password: localStorage.getItem("password"),
      })
      .then((res) => {
        console.log(res.data);
        localStorage.setItem("accnum", res.data.accountNumber);
        this.handleLogin();
      })
      .catch((err) => {
        console.log("Failed to Login");
        const message = this.createChatBotMessage("Failed to Login...");
        this.updateChatbotState(message);
      });
  };

  checkBalance = async () => {
    console.log("Hii clicked Balance....");
    await axios
      .post("http://localhost:8181/account/balancecheck", {
        accountNumber: localStorage.getItem("accnum"),
      })
      .then((res) => {
        console.log(res.data);
        const message = this.createChatBotMessage(res.data);

        this.updateChatbotState(message);
      })
      .catch((err) => {
        console.log("Failed to get account details");
        const message = this.createChatBotMessage(
          "Failed to fetch account details..."
        );

        this.updateChatbotState(message);
      });
  };
}

export default ActionProvider;
