import React from "react";
import { createChatBotMessage } from "react-chatbot-kit";
import CheckBalance from "./CheckBalance";
import LearningOptions from "./LearningOptions";
import LinkList from "./LinkList";

const config = {
  botName: "JARVIS",
  initialMessages: [
    createChatBotMessage("Hi, I'm here to help. Please Login to continue.", {
      widget: "learningOptions",
    }),
  ],
  widgets: [
    {
      widgetName: "learningOptions",
      widgetFunc: (props) => <LearningOptions {...props} />,
    },
    {
      widgetName: "checkbalance",
      widgetFunc: (props) => <CheckBalance {...props} />,
      props: {
        options: [
          {
            text: "Check balance",
            id: 1,
          },
        ],
      },
    },
  ],
  customStyles: {
    botMessageBox: {
      backgroundColor: "#376B7E",
    },
    chatButton: {
      backgroundColor: "#376B7E",
    },
  },
};

export default config;
