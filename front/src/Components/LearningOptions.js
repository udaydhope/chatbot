import React from "react";
import TextField from "@material-ui/core/TextField";
import "./LearningOptions.css";
import { Button } from "@material-ui/core";

const LearningOptions = (props) => {
  const options = [
    {
      text: "Username",
      handler: () => {},
      id: 1,
    },
    { text: "Password", handler: () => {}, id: 2 },
    {
      text: "Login",
      handler: props.actionProvider.login,
      id: 3,
    },
  ];

  const optionsMarkup = options.map((option) =>
    option.text === "Username" ? (
      <TextField
        id="standard-basic"
        label="Username"
        style={{ marginTop: "5px", fontSize: 6 }}
        onChange={(e) => props.actionProvider.setUsername(e.target.value)}
      />
    ) : option.text === "Password" ? (
      <TextField
        id="standard-basic"
        label="Password"
        type="password"
        style={{ marginTop: "5px", fontSize: 6 }}
        onChange={(e) => props.actionProvider.setPassword(e.target.value)}
      />
    ) : (
      <Button
        color="primary"
        variant="contained"
        key={option.id}
        onClick={option.handler}
        style={{ marginTop: "8px" }}
      >
        {option.text}
      </Button>
    )
  );

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default LearningOptions;
