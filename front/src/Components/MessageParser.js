import React, { Component } from "react";

class MessageParser {
  constructor(actionProvider, state) {
    this.actionProvider = actionProvider;
    this.state = state;
  }

  parse(message) {
    console.log(message);
    const lowerCaseMessage = message.toLowerCase();

    if (
      lowerCaseMessage.includes("hello") ||
      lowerCaseMessage.includes("please enter your account number")
    ) {
      this.actionProvider.greet();
    }

    if (lowerCaseMessage.includes("login")) {
      this.actionProvider.handleLogin();
    }
    if (
      lowerCaseMessage.includes("balance") ||
      lowerCaseMessage.includes("check my balance") ||
      lowerCaseMessage.includes("my balance")
    ) {
      this.actionProvider.checkBalance();
    }
  }
}

export default MessageParser;
