import React from "react";

import "./LinkList.css";

const CheckBalance = (props) => {
  const options = [
    {
      text: "Check Balance",
      handler: props.actionProvider.checkBalance,
      id: 3,
    },
  ];

  const optionsMarkup = options.map((option) => (
    <li key={option.id} className="link-list-item" onClick={option.handler}>
      <a rel="noopener noreferrer" className="link-list-item-url">
        {option.text}
      </a>
    </li>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default CheckBalance;
