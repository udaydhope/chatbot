import { Fab } from "@material-ui/core";
import React, { Component } from "react";
import Chatbot from "react-chatbot-kit";
import ActionProvider from "./ActionProvider";
import config from "./config";
import MessageParser from "./MessageParser";
import FaceIcon from "@material-ui/icons/Face";
import { Popover } from "antd";

export default class Main extends Component {
  componentDidMount() {
    console.log("Hiiii.......");
  }

  render() {
    const chat = () => {
      return (
        <Chatbot
          config={config}
          actionProvider={ActionProvider}
          messageParser={MessageParser}
        />
      );
    };
    return (
      <div>
        <div style={{ float: "right" }}>
          <Popover content={chat} trigger="click">
            <Fab
              style={{ position: "absolute", right: 20, bottom: 10 }}
              color="primary"
              aria-label="add"
            >
              <FaceIcon />
            </Fab>
          </Popover>
        </div>
      </div>
    );
  }
}
