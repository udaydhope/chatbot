import React, { Component } from "react";
import "./App.css";
import Main from "./Components/Main";
import { Layout } from "antd";
import Background from "./Images/botbackground.png";
const { Header, Content } = Layout;

class App extends Component {
  render() {
    return (
      <Layout className="layout">
        <Header
          style={{
            position: "fixed",
            zIndex: 1,
            padding: "0px",
            width: "100%",
            background: "black",
            height: "4.5vw",
          }}
        />
        <Layout style={{ height: "100vh" }}>
          <Content
            style={{
              backgroundImage: `url(${Background})`,
            }}
          >
            <div style={{ paddingTop: "1%", backgroundColor: "#172F42" }}>
              <Main />
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default App;
