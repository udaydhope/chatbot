package com.chatbot.services.PaymentServiceAPI.repo;

import com.chatbot.services.PaymentServiceAPI.model.AccountInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepo extends JpaRepository<AccountInformation,String> {
    AccountInformation findByemail(String email);

    AccountInformation findByaccountNumber(String accountNumber);
}
