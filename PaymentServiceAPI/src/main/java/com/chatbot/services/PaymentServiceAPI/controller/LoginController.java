package com.chatbot.services.PaymentServiceAPI.controller;

import com.chatbot.services.PaymentServiceAPI.dto.SignInfo;
import com.chatbot.services.PaymentServiceAPI.model.AccountInformation;
import com.chatbot.services.PaymentServiceAPI.repo.LoginRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/signing")
public class LoginController {

    @Autowired
    LoginRepo loginRepo;

    @PostMapping("/login")
    public ResponseEntity<Object> signIn(@RequestBody SignInfo accountInformation) {
        AccountInformation accInformation=null;
        try {
            accInformation = loginRepo.findByemail(accountInformation.getUsername());
            if(accInformation != null) {
                if (accInformation.getEmail().equalsIgnoreCase(accountInformation.getUsername()) &&
                        accInformation.getPassword().equalsIgnoreCase(accountInformation.getPassword())) {
                    return new ResponseEntity<Object>(accInformation, HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>("invalid credentials",HttpStatus.NOT_FOUND);
    }
}
