package com.chatbot.services.PaymentServiceAPI.dto;

import lombok.Data;

@Data
public class BalanceCheck {
    private String accountNumber;
}
