package com.chatbot.services.PaymentServiceAPI.controller;

import com.chatbot.services.PaymentServiceAPI.dto.BalanceCheck;
import com.chatbot.services.PaymentServiceAPI.dto.SignInfo;
import com.chatbot.services.PaymentServiceAPI.model.AccountInformation;
import com.chatbot.services.PaymentServiceAPI.repo.AccountRepo;
import com.chatbot.services.PaymentServiceAPI.repo.LoginRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    LoginRepo loginRepo;

    @PostMapping("/balancecheck")
    public ResponseEntity<Object> signIn(@RequestBody BalanceCheck balanceCheck) {
        AccountInformation accountInformation=null;
        try {
            accountInformation=loginRepo.findByaccountNumber(balanceCheck.getAccountNumber());
            if(accountInformation.getAccountBalance().equalsIgnoreCase("0")){
                return new ResponseEntity<>("you have no outstanding balance",HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>("The balance of account number "+accountInformation.getAccountNumber()+
                " is "+accountInformation.getAccountBalance(),HttpStatus.OK);
    }

}
