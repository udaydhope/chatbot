package com.chatbot.services.PaymentServiceAPI.dto;

import lombok.Data;

@Data
public class SignInfo {
    private String username;
    private String password;
}
