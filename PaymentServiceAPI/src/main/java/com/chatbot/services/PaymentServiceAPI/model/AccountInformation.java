package com.chatbot.services.PaymentServiceAPI.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Accountinfo")
public class AccountInformation {

    @Id
    private String accountNumber;
    private String email;
    private String username;
    private String password;
    private String accountBalance;

}
